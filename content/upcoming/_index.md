---
title: "Upcoming Events"
date: 2023-01-21T13:38:18-07:00
draft: false
cascade:
  featured_image: '/images/chess-brown.jpg'
---

# State Events

Look at the [Utah Chess Association](http://www.utahchess.com/) for information about upcoming state events.

## 9th Annual Salt Lake Open

February 11, 2023

All day event hosted at the University of Utah's Warnock Engineering Building. Visit [the event page](http://www.utahchess.com/event/9th-annual-salt-lake-open-2-11-2023/?instance_id=3031) for more information and to sign up. This event is a four round swiss tournament with a classical time control. There is a $2,000 prize pool split among the four rating sections.
