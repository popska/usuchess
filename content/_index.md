---
title: "USU Chess"

description: "Utah State University Chess Club"
theme_version: '2.8.2'
cascade:
  featured_image: '/images/chess-side.jpg'
---

# Club Meetings

Everyone is welcome, regardless of skill level.

### Where

- Eccles Business Building room 216

### When

- Tuesdays at 4:00 (playing games)
- Fridays at 4:00 (emphasis on learning chess)

# Contact

Join us on [Slack](https://chessusu.slack.com/) or send us an email!

{{< form-contact action="https://formspree.io/popska@disroot.org" >}}
