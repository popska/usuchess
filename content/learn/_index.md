---
title: "Learn and Play"
date: 2023-01-21T12:53:50-07:00
draft: false
cascade:
  featured_image: '/images/chess-hands.jpg'
---

### Play With The Club

Come play with us Tuesdays at 4:00 at Eccles Business Building room 216

### Learn With Us

We meet Fridays at 4:00 to learn chess theory. These lessons are aimed at beginning, intermediate, and advanced players.

### Play Online

[lichess.org](lichess.org)

[chess.com](chess.com)

### Resources For Learning

[Daniel Naroditsky's YouTube speedrun series](https://www.youtube.com/channel/UCHP9CdeguNUI-_nBv_UXBhw)

[Learn basic tactics on lichess](https://lichess.org/practice)
