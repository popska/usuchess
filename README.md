# About

This is the static site for Utah State University chess club.

# Building

See [the hugo quick start guide](https://gohugo.io/getting-started/quick-start/) to see how to build the project.
